function [theta, J_history] = gradientDescent(X, y, theta, alpha, num_iters)
%GRADIENTDESCENT Performs gradient descent to learn theta
%   theta = GRADIENTDESENT(X, y, theta, alpha, num_iters) updates theta by 
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
m = length(y); % number of training examples
J_history = zeros(num_iters, 1);

for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta. 
    %
    % Hint: While debugging, it can be useful to print out the values
    %       of the cost function (computeCost) and gradient here.
    %
    
    thetaT = theta';
    sum = zeros(2, 1);
    for i=1:m
        
        % Get example:
        x = X(i, :)';
    
        % Compute hypothesis:
        h = thetaT * x;

        % Add to the sum:
        sum = sum + (h(1) - y(i)) * x;
        
    end
    
    % Multiply each sum by a scalar (alpha/m):
    result = (alpha / m) .* sum;
    
    % Update theta:
    theta = theta - result;

    % ============================================================

    % Save the cost J in every iteration    
    J_history(iter) = computeCost(X, y, theta);

end

end
