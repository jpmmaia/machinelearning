function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta

% Calculate J:
for i=1:m
    
    % Get example line:
    x = X(i, :)';
    
    % Calculate value of the hypothesis:
    z = theta' * x;
    h = sigmoid(z);
    
    % Calculate an iteration in the cost function:
    result = -y(i) * log(h) - (1 - y(i)) * log(1 - h);
    J = J + result;
    
end
J = J / m;

% Add regularization to J:
n = length(theta);
sum = 0;
for j=2:n
    sum = sum + theta(j) * theta(j);
end
J = J + lambda * sum / (2*m);

% Calculate the gradient:
for i = 1:m
    
    % Get example line:
    x = X(i, :)';
    
    % Calculate value of the hypothesis:
    z = theta' * x;
    h = sigmoid(z);

    % Calculate value of an iteration:
    result = (h - y(i)) * x;
    grad = grad + result;
    
end
grad = grad ./ m;

% Add regularization to grad:
for j=2:n
    grad(j) = grad(j) + lambda * theta(j) / m;
end

% =============================================================

end
